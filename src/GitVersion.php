<?php

namespace Drupal\git_version;

/**
 * Class GitVersion
 * @package Drupal\git_version
 */
class GitVersion {

  /**
   * @var string
   *   The path to the .git directory we want to inspect in relation to index.php.
   */
  private $gitPath;

  /**
   * @var array
   *    An array that stores the information about the git repo.
   */
  private $gitInfo;

  /**
   * @param $gitPath
   *   Set the path to the .git directory we want to inspect.
   */
  public function setGitPath($gitPath) {
    $this->gitPath = $gitPath;
  }

  /**
   * @return string
   *   Get the path to the .git directory we want to inspect.
   */
  public function gitPath() {
    return $this->gitPath;
  }

  /**
   * @return string
   *   Get the commit hash.
   */
  public function getHash() {
    return $this->getInfo()['hash'];
  }

  /**
   * @return string
   *   Get the Branch or Tag.
   */
  public function getBranchTag() {
    return $this->getInfo()['branch'];
  }

  public function getAllInfo() {
    $storage = \Drupal::entityTypeManager()->getStorage('git_repo');
    $repos = $storage->loadMultiple();
    $results = [];

    foreach ($repos as $id => $repo) {
      $this->setGitPath($repo->path());

      $branch = $this->getBranchTag();
      if (empty($branch)) $branch = '<em>Repo not found.</em>';

      $hash = $this->getHash();

      $results[$repo->label()]['branch'] = $branch;
      $results[$repo->label()]['hash'] = $hash;
    }

    return $results;
  }

  /**
   * @return array
   *   Get the branch/tag and commit hash in an array. It also sets the
   *   $this->getInfo variable.
   */
  public function getInfo($path = NULL) {
    if (!empty($this->getInfo)) {
      return $this->getInfo;
    }

    if (is_null($path)) $path = $this->gitPath;

    $info['branch'] = '';
    $info['hash'] = '';

    if (file_exists($path . '/HEAD') && $gitStr = trim(file_get_contents($path . '/HEAD'))) {
      // Extract the branch name from the line.
      // Example line:
      //   ref: refs/heads/D8-2053
      $info['branch'] = rtrim(preg_replace("/(.*?\/){2}/", '', $gitStr));

      // Get the hash based on the branch name we got.
      $gitPathBranch = $path . '/refs/heads/' . $info['branch'];

      if (file_exists($gitPathBranch)) {
        $info['hash'] = file_get_contents($gitPathBranch);
      } else {
        // If the file doesn't exist, it may be a tag.
        // This means what we got for the branch name is really a hash.
        $info['hash'] = $info['branch'];
        $info['branch'] = '';

        // Search the .git/packed-refs file because .git/refs/tags doesn't always
        // have the data we need.
        if (file_exists($path . '/packed-refs')) {
          $packed_refs_data = file_get_contents($path . '/packed-refs');
          $packed_refs = explode("\n", $packed_refs_data);

          foreach ($packed_refs as $key => $line) {
            // Tags for a commit are found with a line starting with ^
            if ($line == '^' . $info['hash']) {
              // The tag name is found in one of the previous lines. Go back until
              // until we dont see a line that starts with a ^ and the tag name
              // can be found there.
              // Example File structure:
              //   09db80a4106f17be6c821ba31f08de904066bdb8 refs/tags/1.2.9
              //   ^d972b9c62e9e4f21cbc166d55b0f4cbabff604d2
              $go = TRUE;
              for ($i = $key; $go == TRUE; $i--) {
                if (substr($packed_refs[$i], 0, 1) != '^') {
                  // Extract the tag name from the line.
                  // Example line:
                  //   09db80a4106f17be6c821ba31f08de904066bdb8 refs/tags/1.2.9
                  $info['branch'] = rtrim(preg_replace("/(.*?\/){2}/", '', $packed_refs[$i]));
                  $go = FALSE;
                }
                // If we iterated back to the start of the file, it's time to
                // stop and give up looking for a branch or tag name.
                else if ($i == 0) {
                  $info['branch'] = $info['hash'];
                  $go = FALSE;
                }
              }
            }
          }
        }
      }
    }

    // Trim any whitespace that may have come along when parsing.
    $info['branch'] = trim($info['branch']);
    $info['hash'] = trim($info['hash']);

    $this->gitInfo = $info;
    return $info;
  }
}
