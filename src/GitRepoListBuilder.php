<?php

namespace Drupal\git_version;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Git Repo entities.
 */
class GitRepoListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Git Repo');
    $header['id'] = $this->t('Machine name');
    $header['path'] = $this->t('Path');
    $header['info'] = $this->t('Info');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $info = $entity->info();

    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['path'] = $entity->path();
    $row['info'] = new FormattableMarkup(
      '@branch @ <span title="@fullhash">@hash</span>', [
        '@hash' => substr($info['hash'], 0, 7),
        '@branch' => $info['branch'],
        '@fullhash' => $info['hash'],
      ]
    );
    return $row + parent::buildRow($entity);
  }
}
