<?php

namespace Drupal\git_version\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Git Repo entity.
 *
 * @ConfigEntityType(
 *   id = "git_repo",
 *   label = @Translation("Git Repo"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\git_version\GitRepoListBuilder",
 *     "form" = {
 *       "add" = "Drupal\git_version\Form\GitRepoForm",
 *       "edit" = "Drupal\git_version\Form\GitRepoForm",
 *       "delete" = "Drupal\git_version\Form\GitRepoDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\git_version\GitRepoHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "git_repo",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "path" = "path",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/development/git_repo/{git_repo}",
 *     "add-form" = "/admin/config/development/git_repo/add",
 *     "edit-form" = "/admin/config/development/git_repo/{git_repo}/edit",
 *     "delete-form" = "/admin/config/development/git_repo/{git_repo}/delete",
 *     "collection" = "/admin/config/development/git_repo"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "path",
 *     "uuid"
 *   }
 * )
 */
class GitRepo extends ConfigEntityBase implements GitRepoInterface {

  /**
   * The Git Repo ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Git Repo label.
   *
   * @var string
   */
  protected $label;

  /**
   * The path to the .git folder.
   *
   * @var string
   */
  protected $path;

  /**
   * @inheritDoc.
   */
  public function path() {
    return $this->path;
  }

  /**
   * Get the Git information for this repo.
   *
   * @return array
   */
  public function info() {
    // @TODO: Dependency Injection.
    $gitVersion_service = \Drupal::service('git_version.git_version');
    return $gitVersion_service->getInfo($this->path());
  }
}
