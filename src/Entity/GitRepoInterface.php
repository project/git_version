<?php

namespace Drupal\git_version\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Git Repo entities.
 */
interface GitRepoInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
