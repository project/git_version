<?php

namespace Drupal\git_version\Plugin\Block;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block that shows Git Repo information.
 *
 * @Block(
 *   id = "git_version_block",
 *   admin_label = @Translation("Git Information"),
 * )
 */
class GitVersionBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * Git Version service.
   */
  protected $git_version;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, $git_version) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->git_version = $git_version;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('git_version.git_version')
    );
  }
  /**
   * {@inheritDoc}
   */
  public function build()
  {
    $results = $this->git_version->getAllInfo();

    $value = '<ul>';
    foreach ($results as $label => $result) {
      $value .= '<li>';
      $value .= new FormattableMarkup('<strong>@label</strong>: ', ['@label' => $label]);
      $value .= new FormattableMarkup('@branch @ <span title="@hash">@short_hash</span>', [
        '@branch' => $result['branch'],
        '@hash' => $result['hash'],
        '@short_hash' => substr($result['hash'], 0, 7)
      ]);
      $value .= '</li>';
    }
    $value .= '</ul>';

    // Provide a configuration link if the user has permission.
    $user = \Drupal::currentUser();
    if ($user->hasPermission('administer site configuration')) {
      $value .= Link::createFromRoute('Config', 'entity.git_repo.collection')->toString();
    }

    return [
      '#markup' => $value,
    ];
  }
}
