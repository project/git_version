<?php

namespace Drupal\git_version\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GitRepoForm.
 */
class GitRepoForm extends EntityForm {

  /**
   * Git Version service.
   */
  protected $git_version;

  public function __construct($git_version) {
    $this->git_version = $git_version;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('git_version.git_version')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $git_repo = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $git_repo->label(),
      '#description' => $this->t("Label for the Git Repo."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $git_repo->id(),
      '#machine_name' => [
        'exists' => '\Drupal\git_version\Entity\GitRepo::load',
      ],
      '#disabled' => !$git_repo->isNew(),
    ];

    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path'),
      '#default_value' => $git_repo->path(),
      '#description' => $this->t('Path to the .git directory relative to index.php.<br> <strong>Examples:</strong><br>&nbsp;&nbsp;<code>../.git</code> for a composer project<br>&nbsp;&nbsp;<code>modules/contrib/git_version/.git</code> for a contrib module.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $git_repo = $this->entity;
    $status = $git_repo->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Git Repo.', [
          '%label' => $git_repo->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Git Repo.', [
          '%label' => $git_repo->label(),
        ]));
    }
    $form_state->setRedirectUrl($git_repo->toUrl('collection'));
  }

  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    // Make sure we find a git repo at the provided path.
    $path = $form_state->getValue('path');

    $this->git_version->setGitPath($path);
    $info = $this->git_version->getInfo();

    if (empty($info['hash'])) {
      $form_state->setErrorByName('path', $this->t('We couldn\'t find a Git repo at @path.', ['@path' => $path]));
    }

    parent::validateForm($form, $form_state);
  }

}
