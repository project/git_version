# git_version
This module allows you to display the Git branch or tag of a project in the UI without using exec().

## Installation and Configuration
Install this module like you would any other Drupal module.

In order to find the Git information, the module will need to know where the `.git` directory is in relation to Drupal's `index.php` file. By default, this will be `../.git`. You can change this to look for a `.git` directory in a different place, such as a module.
